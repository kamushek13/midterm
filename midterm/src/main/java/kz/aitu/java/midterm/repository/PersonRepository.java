package kz.aitu.java.midterm.repository;

import kz.aitu.java.midterm.entity.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository <Person, Long>
{
}
