package kz.aitu.java.midterm;

import org.springframework.stereotype.Component;
import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@Component
public class PersonFilter implements Filter {
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE");
        chain.doFilter(req, res);
        try {
            chain.doFilter(req, res);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("filtered!");
        }
    }
    public void init(FilterConfig filterConfig) {}
    public void destroy() {}
}